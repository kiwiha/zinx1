package main

import (
	"fmt"
	"net"
	"time"
)

func main() {
	fmt.Println("client start...")

	time.Sleep(5 * time.Second)

	//直接进行连接
	conn, err := net.Dial("tcp", "127.0.0.1:8999")
	if err != nil {
		fmt.Println("client start err", err)
		return
	}
	for {
		///2.链接调用Write写数据
		_, err := conn.Write([]byte("Hello,I am Client"))
		if err != nil {
			fmt.Println("write conn err", err)
			return
		}
		time.Sleep(5 * time.Second)

		buf := make([]byte, 512)
		cnt, err := conn.Read(buf)
		if err != nil {
			fmt.Println("read buf err")
			return
		}
		fmt.Printf("Server writer buf %s cnt %d \n", buf, cnt)
	}
}
